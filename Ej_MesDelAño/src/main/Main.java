package main;
import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		System.out.println("Averig�e en qu� mes est� introduciendo un n�mero del 1 al 12");
		int mes= in.nextInt();
		if (mes==1){
			System.out.println("Es el mes de Enero");
		}
		else if (mes==2){
			System.out.println("Es el mes de Febrero");
		}
		else if (mes==3){
			System.out.println("Es el mes de Marzo");
		}
		else if (mes==4){
			System.out.println("Es el mes de Abril");
		}
		else if (mes==5){
			System.out.println("Es el mes de Mayo");
		}
		else if (mes==6){
			System.out.println("Es el mes de Junio");
		}
		else if (mes==7){
			System.out.println("Es el mes de Julio");
		}
		else if (mes==8){
			System.out.println("Es el mes de Agosto");
		}
		else if (mes==9){
			System.out.println("Es el mes de Septiembre");
		}
		else if (mes==10){
			System.out.println("Es el mes de Octubre");
		}
		else if (mes==11){
			System.out.println("Es el mes de Noviembre");
		}
		else if (mes==12){
			System.out.println("Es el mes de Diciembre");
		}
		else{
			System.out.println("Error, introduce un n�mero entre el 1 y el 12 incluidos");
		}
		
		in.close();
	}

}
